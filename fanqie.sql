/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50635
Source Host           : localhost:3306
Source Database       : fanqie

Target Server Type    : MYSQL
Target Server Version : 50635
File Encoding         : 65001

Date: 2019-10-12 00:54:00
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `ct_address`
-- ----------------------------
DROP TABLE IF EXISTS `ct_address`;
CREATE TABLE `ct_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `names` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `detailed` varchar(255) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  `province` varchar(50) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `district` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`)
) ENGINE=MyISAM AUTO_INCREMENT=46 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of ct_address
-- ----------------------------

-- ----------------------------
-- Table structure for `ct_admin_menu`
-- ----------------------------
DROP TABLE IF EXISTS `ct_admin_menu`;
CREATE TABLE `ct_admin_menu` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `groupkey` varchar(20) DEFAULT '0' COMMENT '菜单分组key',
  `name` varchar(10) NOT NULL DEFAULT '' COMMENT '名称',
  `url` varchar(100) NOT NULL DEFAULT '' COMMENT '链接地址',
  `pid` smallint(4) DEFAULT '0' COMMENT '父菜单',
  `ishow` tinyint(1) DEFAULT '1' COMMENT '0：不显示，1：显示，默认为1',
  `sort_num` smallint(3) DEFAULT '0' COMMENT '排序数字',
  `add_time` int(11) DEFAULT '0' COMMENT '添加时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COMMENT='系统后台菜单表';

-- ----------------------------
-- Records of ct_admin_menu
-- ----------------------------
INSERT INTO `ct_admin_menu` VALUES ('1', 'system', '系统管理', '#', '0', '1', '1', '1431589378');
INSERT INTO `ct_admin_menu` VALUES ('2', 'system', '系统配置', '/adminct/config/index', '1', '1', '2', '1431593502');
INSERT INTO `ct_admin_menu` VALUES ('3', 'system', '菜单管理', '/adminct/menu/index', '1', '1', '3', '1431595612');
INSERT INTO `ct_admin_menu` VALUES ('4', 'system', '菜单分组', '/adminct/menuGroup/index', '1', '1', '4', '1431597951');
INSERT INTO `ct_admin_menu` VALUES ('9', 'user', '会员管理', '#', '0', '1', '1', '1469583671');
INSERT INTO `ct_admin_menu` VALUES ('10', 'user', '会员分组', '/adminct/usergroup/index', '9', '1', '2', '1469584030');
INSERT INTO `ct_admin_menu` VALUES ('11', 'news', '文章管理', '#', '0', '1', '1', '1469585484');
INSERT INTO `ct_admin_menu` VALUES ('12', 'user', '会员列表', '/adminct/user/index', '9', '1', '3', '1469601342');
INSERT INTO `ct_admin_menu` VALUES ('13', 'news', '文章分类', '/adminct/newscat/index', '11', '1', '2', '1470726838');
INSERT INTO `ct_admin_menu` VALUES ('14', 'news', '文章列表', '/adminct/news/index', '11', '1', '3', '1470728442');
INSERT INTO `ct_admin_menu` VALUES ('15', 'system', '前端菜单', '/adminct/qmenu/index', '1', '1', '5', '1475709478');
INSERT INTO `ct_admin_menu` VALUES ('16', 'shop', '店铺管理', '#', '0', '1', '1', '1476675812');
INSERT INTO `ct_admin_menu` VALUES ('17', 'shop', '店铺列表', '/adminct/shop/index', '16', '1', '2', '1476675902');
INSERT INTO `ct_admin_menu` VALUES ('18', 'system', '首页轮播图', '/adminct/banner/index', '1', '1', '5', '1478766386');
INSERT INTO `ct_admin_menu` VALUES ('19', 'goods', '商品管理', '#', '0', '1', '1', '1481426832');
INSERT INTO `ct_admin_menu` VALUES ('20', 'wechat', '微信管理', '#', '0', '1', '1', '1481427257');
INSERT INTO `ct_admin_menu` VALUES ('21', 'user', '会员配置', 'adminct/user/config', '9', '1', '4', '1482138708');
INSERT INTO `ct_admin_menu` VALUES ('22', 'goods', '商品分类', '/adminct/goods_cat/index', '19', '1', '2', '1486712697');
INSERT INTO `ct_admin_menu` VALUES ('23', 'goods', '商品列表', '/adminct/goods/index', '19', '1', '3', '1486712765');
INSERT INTO `ct_admin_menu` VALUES ('25', 'order', '订单管理', '#', '0', '1', '1', '1489914046');
INSERT INTO `ct_admin_menu` VALUES ('26', 'order', '订单列表', 'adminct/order/index', '25', '1', '2', '1489914068');
INSERT INTO `ct_admin_menu` VALUES ('27', 'order', '评价审核', 'adminct/comment/index', '25', '1', '2', '1489914267');
INSERT INTO `ct_admin_menu` VALUES ('28', 'wechat', '微信配置', 'adminct/wechat/config', '20', '1', '2', '1497860880');
INSERT INTO `ct_admin_menu` VALUES ('29', 'wechat', '微信菜单', 'adminct/wechat/menu', '20', '1', '3', '1497860993');

-- ----------------------------
-- Table structure for `ct_admin_menu_group`
-- ----------------------------
DROP TABLE IF EXISTS `ct_admin_menu_group`;
CREATE TABLE `ct_admin_menu_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tkey` varchar(20) DEFAULT '' COMMENT '分组key,需要保持唯一',
  `name` varchar(20) DEFAULT '' COMMENT '分组名称',
  `icon` varchar(16) DEFAULT '' COMMENT '分组图标',
  `sort_num` smallint(3) DEFAULT '0' COMMENT '排序数字',
  `add_time` int(11) DEFAULT '0' COMMENT '添加时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='后台菜单分组';

-- ----------------------------
-- Records of ct_admin_menu_group
-- ----------------------------
INSERT INTO `ct_admin_menu_group` VALUES ('1', 'system', '系统', 'cog', '1', '1431586979');
INSERT INTO `ct_admin_menu_group` VALUES ('2', 'user', '会员', 'user', '2', '1469580268');
INSERT INTO `ct_admin_menu_group` VALUES ('3', 'news', '文章', 'newspaper-o', '3', '1469581929');
INSERT INTO `ct_admin_menu_group` VALUES ('4', 'shop', '店铺', 'shopping-bag', '4', '1476675786');
INSERT INTO `ct_admin_menu_group` VALUES ('5', 'goods', '商品', 'shopping-cart', '5', '1481424623');
INSERT INTO `ct_admin_menu_group` VALUES ('6', 'wechat', '微信', 'wechat', '7', '1481424704');
INSERT INTO `ct_admin_menu_group` VALUES ('7', 'order', '订单', 'indent', '6', '1489913872');

-- ----------------------------
-- Table structure for `ct_admin_role`
-- ----------------------------
DROP TABLE IF EXISTS `ct_admin_role`;
CREATE TABLE `ct_admin_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(20) DEFAULT '' COMMENT '角色名称',
  `summary` varchar(100) DEFAULT '' COMMENT '角色简介',
  `permissions` text COMMENT '角色权限的序列表数组',
  `add_time` int(11) DEFAULT '0' COMMENT '添加时间',
  `sort_num` smallint(3) DEFAULT '0' COMMENT '排序数字',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='管理员角色';

-- ----------------------------
-- Records of ct_admin_role
-- ----------------------------
INSERT INTO `ct_admin_role` VALUES ('1', '超级管理员', '超级管理员，具有最高权限。', '%7B%22config%40index%22%3A%221%22%2C%22config%40insert%22%3A%221%22%2C%22config%40update%22%3A%221%22%2C%22config%40delete%22%3A%221%22%2C%22menu%40index%22%3A%221%22%2C%22menu%40insert%22%3A%221%22%2C%22menu%40update%22%3A%221%22%2C%22menu%40quicksave%22%3A%221%22%2C%22menu%40delete%22%3A%221%22%2C%22menuGroup%40index%22%3A%221%22%2C%22menuGroup%40insert%22%3A%221%22%2C%22menuGroup%40update%22%3A%221%22%2C%22menuGroup%40quicksave%22%3A%221%22%2C%22menuGroup%40delete%22%3A%221%22%2C%22admin%40index%22%3A%221%22%2C%22admin%40insert%22%3A%221%22%2C%22admin%40update%22%3A%221%22%2C%22admin%40delete%22%3A%221%22%2C%22role%40index%22%3A%221%22%2C%22role%40insert%22%3A%221%22%2C%22role%40update%22%3A%221%22%2C%22role%40quicksave%22%3A%221%22%2C%22role%40permission%22%3A%221%22%2C%22role%40updatePermission%22%3A%221%22%2C%22role%40delete%22%3A%221%22%2C%22sTemplate%40index%22%3A%221%22%2C%22sTemplate%40insert%22%3A%221%22%2C%22sTemplate%40update%22%3A%221%22%2C%22sTemplate%40delete%22%3A%221%22%2C%22chanel%40index%22%3A%221%22%2C%22chanel%40insert%22%3A%221%22%2C%22chanel%40update%22%3A%221%22%2C%22chanel%40quicksave%22%3A%221%22%2C%22chanel%40delete%22%3A%221%22%2C%22friendLink%40index%22%3A%221%22%2C%22friendLink%40insert%22%3A%221%22%2C%22friendLink%40update%22%3A%221%22%2C%22friendLink%40quicksave%22%3A%221%22%2C%22friendLink%40delete%22%3A%221%22%2C%22userGroup%40index%22%3A%221%22%2C%22userGroup%40insert%22%3A%221%22%2C%22userGroup%40update%22%3A%221%22%2C%22userGroup%40deletes%22%3A%221%22%2C%22userGroup%40delete%22%3A%221%22%2C%22userRole%40index%22%3A%221%22%2C%22userRole%40insert%22%3A%221%22%2C%22userRole%40update%22%3A%221%22%2C%22userRole%40deletes%22%3A%221%22%2C%22userRole%40delete%22%3A%221%22%2C%22user%40index%22%3A%221%22%2C%22user%40insert%22%3A%221%22%2C%22user%40update%22%3A%221%22%2C%22user%40abort%22%3A%221%22%2C%22user%40unaborted%22%3A%221%22%2C%22user%40quickcheck%22%3A%221%22%2C%22user%40deletes%22%3A%221%22%2C%22user%40delete%22%3A%221%22%2C%22user%40sysmsg%22%3A%221%22%2C%22article%40index%22%3A%221%22%2C%22article%40insert%22%3A%221%22%2C%22article%40update%22%3A%221%22%2C%22article%40delete%22%3A%221%22%2C%22articlecat%40index%22%3A%221%22%2C%22articlecat%40insert%22%3A%221%22%2C%22articlecat%40update%22%3A%221%22%2C%22articlecat%40delete%22%3A%221%22%7D', '1431505208', '1');

-- ----------------------------
-- Table structure for `ct_admin_user`
-- ----------------------------
DROP TABLE IF EXISTS `ct_admin_user`;
CREATE TABLE `ct_admin_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `role_id` int(11) unsigned DEFAULT '0' COMMENT '角色ID',
  `username` varchar(30) DEFAULT '' COMMENT '用户名',
  `name` varchar(20) DEFAULT '' COMMENT '姓名',
  `password` varchar(32) DEFAULT '' COMMENT '密码',
  `last_login_time` int(11) DEFAULT '0' COMMENT '最后一次登录时间',
  `last_login_ip` varchar(15) DEFAULT '' COMMENT '最后一次登录ip',
  `add_time` int(11) DEFAULT '0' COMMENT '注册时间',
  `status` tinyint(1) unsigned DEFAULT '1' COMMENT '当前状态(0=>禁用，1=>启用)',
  `isystem` tinyint(1) DEFAULT '0' COMMENT '是否系统管理员(系统管理员不准许删除)',
  `summary` varchar(100) DEFAULT '' COMMENT '管理员简介',
  `encrypt` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='管理员表';

-- ----------------------------
-- Records of ct_admin_user
-- ----------------------------
INSERT INTO `ct_admin_user` VALUES ('1', '1', 'admin', '管理员', 'b59d796c406124dda60b362f1cf3434f', '1465696126', '127.0.0.1', '1431566261', '1', '1', 'I\'m  a super manager, own all the privileages.', 'TPhIUlN');

-- ----------------------------
-- Table structure for `ct_banner`
-- ----------------------------
DROP TABLE IF EXISTS `ct_banner`;
CREATE TABLE `ct_banner` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `thumb` varchar(255) DEFAULT NULL,
  `mark` varchar(255) DEFAULT NULL COMMENT '备注',
  `addtime` int(11) DEFAULT NULL,
  `summary` varchar(255) DEFAULT NULL COMMENT '简介',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='轮播图';


-- ----------------------------
-- Table structure for `ct_comment`
-- ----------------------------
DROP TABLE IF EXISTS `ct_comment`;
CREATE TABLE `ct_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `gid` int(11) DEFAULT NULL,
  `oid` int(11) DEFAULT NULL,
  `content` text,
  `addtime` int(11) DEFAULT NULL,
  `state` int(11) DEFAULT '0' COMMENT '状态 0 不通过 1通过',
  PRIMARY KEY (`id`),
  KEY `productid` (`gid`),
  KEY `orderid` (`oid`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;


-- ----------------------------
-- Table structure for `ct_config`
-- ----------------------------
DROP TABLE IF EXISTS `ct_config`;
CREATE TABLE `ct_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cname` varchar(50) DEFAULT NULL COMMENT '变量title',
  `tkey` varchar(50) DEFAULT NULL COMMENT '变量分组key',
  `key` varchar(50) DEFAULT NULL COMMENT '变量key',
  `val` varchar(250) DEFAULT NULL COMMENT '变量值',
  `bak` varchar(250) DEFAULT NULL COMMENT '提示内容',
  `imports` varchar(20) DEFAULT NULL COMMENT '输入方式 input  radio',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='系统配置表';


-- ----------------------------
-- Table structure for `ct_earnings_detail`
-- ----------------------------
DROP TABLE IF EXISTS `ct_earnings_detail`;
CREATE TABLE `ct_earnings_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT '0' COMMENT '收益人ID',
  `cid` int(11) DEFAULT '0' COMMENT '收益来源id',
  `money` decimal(10,2) DEFAULT '0.00',
  `addtime` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户收益明细表';


-- ----------------------------
-- Table structure for `ct_goods`
-- ----------------------------
DROP TABLE IF EXISTS `ct_goods`;
CREATE TABLE `ct_goods` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `names` varchar(100) NOT NULL COMMENT '产品名字',
  `code` varchar(100) DEFAULT '0' COMMENT '货号',
  `putaway` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0下架 1上架',
  `catid` int(11) NOT NULL COMMENT '分类id',
  `price` decimal(10,2) DEFAULT '0.00' COMMENT '实际售价',
  `baz_price` decimal(10,2) DEFAULT '0.00' COMMENT '市场价格',
  `weight` decimal(10,2) DEFAULT '0.00' COMMENT '重量',
  `stock` int(10) DEFAULT '0' COMMENT '库存',
  `sales` int(10) DEFAULT '0' COMMENT '销量',
  `is_stock` tinyint(1) DEFAULT '0' COMMENT '0 拍下减库存，1用不减库存',
  `is_recommand` tinyint(1) DEFAULT '0' COMMENT '首页推荐',
  `is_new` tinyint(1) DEFAULT '0' COMMENT '新品',
  `is_first` tinyint(1) DEFAULT '0' COMMENT '首发',
  `is_hot` tinyint(1) DEFAULT '0' COMMENT '热卖',
  `is_jingping` tinyint(1) DEFAULT '0' COMMENT '精品',
  `is_freight` tinyint(1) DEFAULT '0' COMMENT '是否包邮 0 免运费  1需要运费',
  `promotion` tinyint(1) DEFAULT '0' COMMENT '促销  0 关闭  1开启',
  `p_start_time` int(11) DEFAULT NULL COMMENT '促销开始时间',
  `p_end_time` int(11) DEFAULT NULL COMMENT '促销结束时间',
  `integral` int(11) DEFAULT '0' COMMENT '积分',
  `thumb` varchar(255) DEFAULT NULL,
  `thumb_arr` varchar(255) DEFAULT NULL,
  `share_describe` varchar(255) DEFAULT NULL COMMENT '分享描述',
  `content` text COMMENT '商品内容',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `is_sku` tinyint(1) DEFAULT '0' COMMENT '是否启用规格',
  `sku_titles` varchar(255) DEFAULT NULL COMMENT '规格标题',
  `sku_options` varchar(500) DEFAULT NULL COMMENT '规格属性',
  `sku_price` varchar(500) DEFAULT NULL COMMENT '规格价格',
  `sku_stock` varchar(500) DEFAULT NULL COMMENT '规格库存',
  `shopid` int(11) DEFAULT '0' COMMENT '店铺的ID',
  `addtime` int(11) DEFAULT '0' COMMENT '添加时间',
  `sku_paths` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for `ct_goods_cat`
-- ----------------------------
DROP TABLE IF EXISTS `ct_goods_cat`;
CREATE TABLE `ct_goods_cat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `names` varchar(50) NOT NULL COMMENT '分类名称',
  `thumb` varchar(255) NOT NULL COMMENT '分类图片',
  `thumbadv` varchar(255) NOT NULL COMMENT '分类广告图片',
  `thumbadvurl` varchar(255) NOT NULL COMMENT '分类广告url',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上级分类ID,0为第一级',
  `isrecommand` tinyint(1) NOT NULL DEFAULT '0' COMMENT '推荐',
  `description` varchar(500) NOT NULL COMMENT '分类介绍',
  `sort` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否开启',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for `ct_image`
-- ----------------------------
DROP TABLE IF EXISTS `ct_image`;
CREATE TABLE `ct_image` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `userid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `thumb` varchar(100) NOT NULL DEFAULT '',
  `filesize` int(11) NOT NULL COMMENT '文件大小',
  `addtime` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间',
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=373 DEFAULT CHARSET=utf8 COMMENT='图片管理';

-- ----------------------------
-- Table structure for `ct_news`
-- ----------------------------
DROP TABLE IF EXISTS `ct_news`;
CREATE TABLE `ct_news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `content` text COMMENT '内容',
  `summary` varchar(255) DEFAULT NULL COMMENT '摘要',
  `thumb` varchar(255) DEFAULT NULL COMMENT '图片',
  `hits` int(11) DEFAULT '0' COMMENT '访问量',
  `catid` int(11) DEFAULT NULL COMMENT '分类ID',
  `addtime` int(11) DEFAULT NULL COMMENT '添加时间',
  `buy_url` varchar(255) DEFAULT NULL,
  `state` int(11) NOT NULL DEFAULT '0' COMMENT '1热点 2推荐',
  `shopid` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for `ct_newscat`
-- ----------------------------
DROP TABLE IF EXISTS `ct_newscat`;
CREATE TABLE `ct_newscat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `catname` varchar(255) DEFAULT NULL,
  `thumb` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL COMMENT '别名',
  `keywords` varchar(255) DEFAULT NULL COMMENT '关键字',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `addtime` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for `ct_news_comment`
-- ----------------------------
DROP TABLE IF EXISTS `ct_news_comment`;
CREATE TABLE `ct_news_comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `nickname` varchar(50) DEFAULT NULL,
  `thumb` varchar(255) DEFAULT NULL,
  `nid` int(11) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='文章评价';


-- ----------------------------
-- Table structure for `ct_order`
-- ----------------------------
DROP TABLE IF EXISTS `ct_order`;
CREATE TABLE `ct_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_no` varchar(100) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL COMMENT '用户id',
  `price` decimal(10,2) DEFAULT NULL COMMENT '实际支付价格',
  `addtime` int(11) DEFAULT NULL COMMENT '下单时间',
  `state` int(11) DEFAULT '1' COMMENT '状态',
  `pay_time` int(11) DEFAULT '0' COMMENT '支付时间',
  `transaction_id` varchar(50) DEFAULT '' COMMENT '微信返回的订单号',
  `send` int(2) DEFAULT '0' COMMENT '发货记录 对应运单表',
  `buy_name` varchar(255) DEFAULT NULL,
  `buy_mobile` varchar(255) DEFAULT NULL,
  `buy_address` varchar(255) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `shopid` int(11) DEFAULT '0' COMMENT '店铺ID',
  `pay_type` int(2) DEFAULT '0' COMMENT '支付方式记录在根目录',
  `pay_order_no` varchar(100) DEFAULT NULL COMMENT '支付订单号',
  `exp_company` varchar(255) DEFAULT NULL COMMENT '快递公司',
  `exp_no` varchar(255) DEFAULT NULL COMMENT '快递单号',
  `exp_time` int(11) DEFAULT '0' COMMENT '发货时间',
  `confirm_time` int(11) DEFAULT '0' COMMENT '收货时间',
  `openid` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_no` (`order_no`),
  KEY `buyid` (`uid`)
) ENGINE=MyISAM AUTO_INCREMENT=56 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='订单';


-- ----------------------------
-- Table structure for `ct_order_lists`
-- ----------------------------
DROP TABLE IF EXISTS `ct_order_lists`;
CREATE TABLE `ct_order_lists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_no` varchar(100) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `gid` int(11) DEFAULT NULL COMMENT '产品ID',
  `uid` int(11) DEFAULT NULL COMMENT '用户id',
  `prices` decimal(10,2) DEFAULT NULL COMMENT '产品价格',
  `thumb` varchar(255) DEFAULT NULL COMMENT '订单图片',
  `catid` int(11) DEFAULT NULL COMMENT '产品分类',
  `sku` varchar(255) DEFAULT NULL COMMENT '规格',
  `num` int(11) DEFAULT '0' COMMENT '数量',
  `oid` int(11) DEFAULT '0' COMMENT '订单表id',
  PRIMARY KEY (`id`),
  KEY `productid` (`gid`),
  KEY `buyid` (`uid`)
) ENGINE=MyISAM AUTO_INCREMENT=59 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='订单子列表';


-- ----------------------------
-- Table structure for `ct_order_log`
-- ----------------------------
DROP TABLE IF EXISTS `ct_order_log`;
CREATE TABLE `ct_order_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_no` varchar(100) DEFAULT NULL,
  `openid` varchar(100) DEFAULT NULL,
  `update_error` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_no` (`order_no`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='订单日志';

-- ----------------------------
-- Records of ct_order_log
-- ----------------------------
INSERT INTO `ct_order_log` VALUES ('1', null, 'sdf', 'sdf');

-- ----------------------------
-- Table structure for `ct_qmenu`
-- ----------------------------
DROP TABLE IF EXISTS `ct_qmenu`;
CREATE TABLE `ct_qmenu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qname` varchar(50) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `addtime` int(11) DEFAULT NULL,
  `sort_num` int(11) DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `ct_sessions`
-- ----------------------------
DROP TABLE IF EXISTS `ct_sessions`;
CREATE TABLE `ct_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) DEFAULT '',
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `data` text NOT NULL,
  `timestamp` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_activity_idx` (`last_activity`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `ct_shop`
-- ----------------------------
DROP TABLE IF EXISTS `ct_shop`;
CREATE TABLE `ct_shop` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `username` varchar(30) DEFAULT '' COMMENT '用户名',
  `password` varchar(32) DEFAULT '' COMMENT '密码',
  `last_login_time` int(11) DEFAULT '0' COMMENT '最后一次登录时间',
  `last_login_ip` varchar(15) DEFAULT '' COMMENT '最后一次登录ip',
  `add_time` int(11) DEFAULT '0' COMMENT '注册时间',
  `state` tinyint(1) unsigned DEFAULT '1' COMMENT '当前状态(0=>禁用，1=>启用)',
  `encrypt` varchar(50) DEFAULT NULL,
  `thumb` varchar(100) DEFAULT NULL,
  `company` varchar(50) DEFAULT NULL,
  `shop_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='管理员表';


-- ----------------------------
-- Table structure for `ct_shop_income`
-- ----------------------------
DROP TABLE IF EXISTS `ct_shop_income`;
CREATE TABLE `ct_shop_income` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_no` varchar(100) DEFAULT NULL COMMENT '订单号',
  `shopid` int(11) DEFAULT NULL,
  `money` decimal(10,2) DEFAULT '0.00',
  `state` int(11) DEFAULT '0' COMMENT '1 申请提现  2 提现通过 3提现失败',
  `addtime` int(11) DEFAULT '0',
  `mark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='商户收益列表';


-- ----------------------------
-- Table structure for `ct_shop_payout`
-- ----------------------------
DROP TABLE IF EXISTS `ct_shop_payout`;
CREATE TABLE `ct_shop_payout` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `shopid` int(11) DEFAULT NULL,
  `money` decimal(10,0) DEFAULT '0',
  `state` int(11) DEFAULT '0',
  `addtime` int(11) DEFAULT '0',
  `mark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='商户提现表';

-- ----------------------------
-- Records of ct_shop_payout
-- ----------------------------

-- ----------------------------
-- Table structure for `ct_test`
-- ----------------------------
DROP TABLE IF EXISTS `ct_test`;
CREATE TABLE `ct_test` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1890 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of ct_test
-- ----------------------------

-- ----------------------------
-- Table structure for `ct_times`
-- ----------------------------
DROP TABLE IF EXISTS `ct_times`;
CREATE TABLE `ct_times` (
  `times_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `login_ip` char(15) DEFAULT NULL COMMENT 'ip',
  `login_time` int(10) unsigned DEFAULT NULL,
  `group_id` int(10) unsigned DEFAULT NULL,
  `failure_times` int(10) unsigned DEFAULT NULL,
  `is_admin` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`times_id`)
) ENGINE=MyISAM AUTO_INCREMENT=60 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ct_times
-- ----------------------------

-- ----------------------------
-- Table structure for `ct_user`
-- ----------------------------
DROP TABLE IF EXISTS `ct_user`;
CREATE TABLE `ct_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `openid` varchar(50) DEFAULT NULL COMMENT '微信openid',
  `username` varchar(20) DEFAULT NULL COMMENT '用户名',
  `password` varchar(50) DEFAULT NULL,
  `encrypt` varchar(50) DEFAULT NULL COMMENT '密保',
  `nickname` varchar(50) DEFAULT NULL COMMENT '用户昵称',
  `thumb` varchar(255) DEFAULT NULL COMMENT '头像',
  `sex` varchar(2) DEFAULT '女' COMMENT '性别默认女',
  `mobile` varchar(11) DEFAULT '0' COMMENT '手机',
  `groupid` int(11) DEFAULT '1',
  `address` int(11) DEFAULT '0' COMMENT '默认地址',
  `p_1` int(11) DEFAULT '0' COMMENT '一级',
  `p_2` int(11) DEFAULT '0' COMMENT '二级',
  `qrcode` varchar(255) DEFAULT NULL COMMENT '二维码',
  `qrcode_time` int(11) DEFAULT NULL,
  `money` decimal(11,2) DEFAULT '0.00' COMMENT '钱包',
  `addtime` int(11) DEFAULT '0' COMMENT '注册时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `ct_user_group`
-- ----------------------------
DROP TABLE IF EXISTS `ct_user_group`;
CREATE TABLE `ct_user_group` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `gname` varchar(20) DEFAULT '' COMMENT '会员分组名称',
  `summary` varchar(60) DEFAULT '' COMMENT '会员分组简介',
  `addtime` int(11) DEFAULT '0' COMMENT '添加时间',
  `fans_num` int(11) DEFAULT '0' COMMENT '粉丝量',
  `p_1` decimal(11,2) DEFAULT '0.00' COMMENT '一级',
  `p_2` decimal(11,2) DEFAULT '0.00' COMMENT '二级',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='会员分组';


-- ----------------------------
-- Table structure for `ct_withdraw_cash`
-- ----------------------------
DROP TABLE IF EXISTS `ct_withdraw_cash`;
CREATE TABLE `ct_withdraw_cash` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT '0' COMMENT '收益人ID',
  `money` decimal(10,2) DEFAULT '0.00',
  `addtime` int(11) DEFAULT '0',
  `shtime` int(11) DEFAULT NULL COMMENT '审核时间',
  `state` int(11) DEFAULT '0' COMMENT '0 审核中 1通过  2失败',
  `msg` varchar(255) DEFAULT NULL COMMENT '审核说明',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户提现明细表';

-- ----------------------------
-- Records of ct_withdraw_cash
-- ----------------------------
